# Contributing to Claptrap

Contributions are always greatly appreciated. Here are the guidlines I would like to be followed: 

## Code of Conduct

This project will generally follow rust's [code of conduct](https://www.rust-lang.org/en-US/conduct.html). However, here is a highlight 
on some key points. 

  * We are committed to providing a friendly, safe and welcoming environment for all, regardless of level of experience, gender, gender 
    identity and expression, sexual orientation, disability, personal appearance, body size, race, ethnicity, age, religion, nationality, 
    or other similar characteristic.

  * Please be kind and courteous. There’s no need to be mean or rude

  * We will exclude you from interaction if you insult, demean or harass anyone. That is not welcome behaviour. We interpret the term 
    “harassment” as including the definition in the Citizen Code of Conduct; if you have any lack of clarity about what might be included 
    in that concept, please read their definition. In particular, we don’t tolerate behavior that excludes people in socially marginalized 
    groups.

  * Likewise any spamming, trolling, flaming, baiting or other attention-stealing behaviour is not welcome.

## Question or Problem? 
  At this this time there is no irc channel, but feel free to create an issue for questions. 

## Found an Issue? 

If you have found a bug in the source code or documentation, you can help by submitting an issue to this repo! However, when create an 
issue please use a template that best suits the "issue" in question. 

## Want a Feature? 

At this time a just submit an issue to the repo where the feature can be discussed. In the future I hope to have a seperate RFC repo 
to overlook potential features and major changes. 

## Submitting a Merge Request

  * All code should be formatted with the most recent version rustfmt. 
  * Wrap all code at 80 characters

## Git Commit Guidelines

This project will use the angularjs [git commit guideline](https://github.com/angular/angular.js/blob/master/CONTRIBUTING.md#-git-commit-guidelines) with minor revisions. Those guidelines are as follows: 

### Commit Message Format

Each commit message consists of a header, a body and a footer. The header has a special format that includes a type, a scope and a subject:

<type>(<scope>): <subject>
<BLANK LINE>
<body>
<BLANK LINE>
<footer>

The header is mandatory and the scope of the header is optional.

Any line of the commit message cannot be longer 100 characters! This allows the message to be easier to read on GitHub as well as in various git tools.

### Revert

If the commit reverts a previous commit, it should begin with revert:, followed by the header of the reverted commit. In the body it should say: This reverts commit <hash>., where the hash is the SHA of the commit being reverted.
Type

Must be one of the following:

    feat: A new feature
    fix: A bug fix
    docs: Documentation only changes
    style: Changes that do not affect the meaning of the code (white-space, formatting, missing semi-colons, etc)
    refactor: A code change that neither fixes a bug nor adds a feature
    perf: A code change that improves performance
    test: Adding missing or correcting existing tests
    chore: Changes to the build process or auxiliary tools and libraries such as documentation generation

### Scope

The scope could be anything specifying place of the commit change. More specfically use up to the last two levels of the file path and the 
file name itself without the extenstin. For example, "feat(src/modules/mod): add customer module" 

You can use * when the change affects more than a single scope.

### Subject

The subject contains succinct description of the change:

    use the imperative, present tense: "change" not "changed" nor "changes"
    don't capitalize first letter
    no dot (.) at the end

### Body

Just as in the subject, use the imperative, present tense: "change" not "changed" nor "changes". The body should include the motivation for the change and contrast this with previous behavior.
Footer

The footer should contain any information about Breaking Changes and is also the place to reference GitHub issues that this commit closes.

Breaking Changes should start with the word BREAKING CHANGE: with a space or two newlines. The rest of the commit message is then used for this.

A detailed explanation can be found in this document.
